package main

import (
	"bitbucket.org/maxhauser/jsmin"
	"os"
)

func main() {
	jsmin.Run(os.Stdin, os.Stdout)
}
