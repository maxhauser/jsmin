
## This is a direct port of Douglas Crockford's jsmin to go.

### Usage

The `Run` method takes a io.Reader and io.Writer and reads all content from the reader,
minifies it, and writes the output to the writer.

`jsmin.Run(os.Stdin, os.Stdout)`

### Install

Install the library only

`go get bitbucket.org/maxhauser/jsmin`

Install the command and the library

`go get bitbucket.org/maxhauser/jsmin/jsmin`


For copyright notes see inside the file jsmin.go