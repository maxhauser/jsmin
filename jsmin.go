/*
This is a direct port of jsmin.c to go.

Permission is hereby granted to use this version of the library under the
same terms as jsmin.c, which has the following license:

Copyright (c) 2002 Douglas Crockford  (www.crockford.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// A direct port of jsmin.c to go.
package jsmin

import (
	"io"
	"bufio"
	"errors"
)

const eof = -1

var (
	ErrUnterminatedComment        = errors.New("Unterminated comment.")
	ErrUnterminatedStringConstant = errors.New("Unterminated string literal.")
	ErrUnterminatedRegExSet       = errors.New("Unterminated set in Regular Expression literal.")
	ErrUnterminatedRegEx          = errors.New("Unterminated Regular Expression literal.")
)

type state struct {
	theA, theB, theLookahead int

	reader *bufio.Reader
	writer *bufio.Writer
}

// Reads all content from the reader, minifies it and writes it to the writer.
func Run(r io.Reader, w io.Writer) {
	s := state{}
	s.theLookahead = eof

	if reader, ok := r.(*bufio.Reader); ok {
		s.reader = reader
	} else {
		s.reader = bufio.NewReader(r)
	}

	if writer, ok := w.(*bufio.Writer); ok {
		s.writer = writer
	} else {
		s.writer = bufio.NewWriter(w)
	}

	s.jsmin()
}

func (s *state) putc(r int) {
	if err := s.writer.WriteByte(byte(r)); err != nil {
		panic(err)
	}
}

func (s *state) getc() int {
	c, err := s.reader.ReadByte()
	if err == io.EOF {
		s.writer.Flush()
		return eof
	} else if err != nil {
		panic(err)
	}
	return int(c)
}

func isAlphanum(c int) bool {
	return ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') ||
		(c >= 'A' && c <= 'Z') || c == '_' || c == '$' || c == '\\' ||
		c > 126)
}

func (s *state) get() int {
	c := s.theLookahead
	s.theLookahead = eof
	if c == eof {
		c = s.getc()
	}
	if c >= ' ' || c == '\n' || c == eof {
		return c
	}
	if c == '\r' {
		return '\n'
	}
	return ' '
}

func (s *state) peek() int {
	s.theLookahead = s.get()
	return s.theLookahead
}

func (s *state) next() int {
	c := s.get()
	if c == '/' {
		switch s.peek() {
		case '/':
			for {
				c = s.get()
				if c <= '\n' {
					return c
				}
			}
			fallthrough
		case '*':
			s.get()
			for {
				switch s.get() {
				case '*':
					if s.peek() == '/' {
						s.get()
						return ' '
					}
				case eof:
					panic(ErrUnterminatedComment)
				}
			}
			fallthrough
		default:
			return c
		}
	}
	return c
}

func (s *state) action(d int) {
	switch d {
	case 1:
		s.putc(s.theA)
		fallthrough
	case 2:
		s.theA = s.theB
		if s.theA == '\'' || s.theA == '"' || s.theA == '`' {
			for {
				s.putc(s.theA)
				s.theA = s.get()
				if s.theA == s.theB {
					break
				}
				if s.theA == '\\' {
					s.putc(s.theA)
					s.theA = s.get()
				}
				if s.theA == eof {
					panic(ErrUnterminatedStringConstant)
				}
			}
		}
		fallthrough
	case 3:
		s.theB = s.next()
		if s.theB == '/' && (s.theA == '(' || s.theA == ',' || s.theA == '=' ||
			s.theA == ':' || s.theA == '[' || s.theA == '!' ||
			s.theA == '&' || s.theA == '|' || s.theA == '?' ||
			s.theA == '{' || s.theA == '}' || s.theA == ';' ||
			s.theA == '\n') {
			s.putc(s.theA)
			s.putc(s.theB)
			for {
				s.theA = s.get()
				if s.theA == '[' {
					for {
						s.putc(s.theA)
						s.theA = s.get()
						if s.theA == ']' {
							break
						}
						if s.theA == '\\' {
							s.putc(s.theA)
							s.theA = s.get()
						}
						if s.theA == eof {
							panic(ErrUnterminatedRegExSet)
						}
					}
				} else if s.theA == '/' {
					break
				} else if s.theA == '\\' {
					s.putc(s.theA)
					s.theA = s.get()
				}
				if s.theA == eof {
					panic(ErrUnterminatedRegEx)
				}
				s.putc(s.theA)
			}
			s.theB = s.next()
		}
	}
}

func (s *state) jsmin() {
	if s.peek() == 0xEF {
		s.get()
		s.get()
		s.get()
	}
	s.theA = '\n'
	s.action(3)
	for s.theA != eof {
		switch s.theA {
		case ' ':
			if isAlphanum(s.theB) {
				s.action(1)
			} else {
				s.action(2)
			}
		case '\n':
			switch s.theB {
			case '{', '[', '(', '+', '-', '!', '~':
				s.action(1)
			case ' ':
				s.action(3)
			default:
				if isAlphanum(s.theB) {
					s.action(1)
				} else {
					s.action(2)
				}
			}
		default:
			switch s.theB {
			case ' ':
				if isAlphanum(s.theA) {
					s.action(1)
					break
				}
				s.action(3)
			case '\n':
				switch s.theA {
				case '}', ']', ')', '+', '-', '"', '\'', '`':
					s.action(1)
				default:
					if isAlphanum(s.theA) {
						s.action(1)
					} else {
						s.action(3)
					}
				}
			default:
				s.action(1)
			}
		}
	}
}
